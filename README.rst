=========
Termgraph
=========

Working title. Draws boxes and stuff.

Example
^^^^^^^

This is an example of what you can do::

    ╭───────────╮      
    │ thing one ├─────┐    ╭─────────────╮
    │    foo    │     └────┤ thing three │
    ╰──┬─────┬──╯          │     baz     │
       │     ┆             ╰─────────────╯
       │     ┆
       │     ┆
    ╭──┴─────┴──╮
    │ thing two │
    │    bar    │
    ╰───────────╯

with some code that probably looks like this::

   t1 = Box('thing one', 'foo')
   t2 = Box('thing two', 'bar')
   t3 = Box('thing three', 'baz')

   Edge(t1, t2)
   Edge(t1, t2, style=Style.dotted)
   Edge(t1, t3)

References
----------

https://en.wikipedia.org/wiki/Box-drawing_character#Unicode
