#!/usr/bin/make -f

.PHONY: all lint test clean

all: lint test

test:
	python3 -m unittest

lint:
	python3 -m flake8 --ignore=E501

clean:
	rm -rf __pycache__ sequence/__pycache__
