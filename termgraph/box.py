

class Box:
    def __init__(self, *labels):
        self.labels = labels
        self.width = len(max(self.labels)) + 2
        self.height = len(self.labels) + 2

    def get(self):
        yield f"╭{'─'*self.width}╮"
        for label in self.labels:
            yield f"│{label:^{self.width}}│"
        yield f"╰{'─'*self.width}╯"

    def __str__(self):
        return "\n".join(list(self.get()))
