

class Canvas:
    def __init__(self, rows, cols, background=None):
        assert rows > 4, 'This does not even fit one box'
        self.rows = rows
        assert cols > 4, 'This does not even fit one box'
        self.cols = cols
        self.background = " " if background is None else background
        self.elements = []

    def get(self):
        for row in range(self.rows):
            yield self.background * self.cols

    def add(self, element):
        assert element.width <= self.cols, 'Element does not fix'
        assert element.height <= self.rows, 'Element does not fix'
        self.elements.append(element)
