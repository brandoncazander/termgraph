import unittest

from termgraph.canvas import Canvas
from termgraph.box import Box


class CanvasTest(unittest.TestCase):
    def test_validate_size(self):
        invalid_dimensions = [
            (-1, 12),
            (4, 4),
            (5, 4),
            (4, 5)
        ]
        for dim in invalid_dimensions:
            with self.subTest(dim):
                with self.assertRaises(AssertionError):
                    Canvas(*dim)

    def test_blank_canvas(self):
        canvas = Canvas(5, 10, background="░").get()
        self.assertEqual(next(canvas), "░░░░░░░░░░")
        self.assertEqual(next(canvas), "░░░░░░░░░░")
        self.assertEqual(next(canvas), "░░░░░░░░░░")
        self.assertEqual(next(canvas), "░░░░░░░░░░")
        self.assertEqual(next(canvas), "░░░░░░░░░░")
        with self.assertRaises(StopIteration):
            next(canvas)

    def test_box_does_not_fit(self):
        box = Box('really long label')
        canvas = Canvas(5, 5)
        with self.assertRaises(AssertionError):
            canvas.add(box)

    def test_box_just_fits(self):
        box = Box('foo')
        canvas = Canvas(5, 5)
        canvas.add(box)

    def test_canvas_with_box(self):
        canvas = Canvas(5, 9, background="░")
        canvas.add(Box('foo'))
        canvas = canvas.get()
        self.assertEqual(next(canvas), "░░░░░░░░░")
        self.assertEqual(next(canvas), "░╭─────╮░")
        self.assertEqual(next(canvas), "░│ foo │░")
        self.assertEqual(next(canvas), "░╰─────╯░")
        self.assertEqual(next(canvas), "░░░░░░░░░")
        with self.assertRaises(StopIteration):
            next(canvas)
