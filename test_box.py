import unittest

from termgraph.box import Box


class BoxTest(unittest.TestCase):
    def test_short_name(self):
        box = Box('foo').get()
        self.assertEqual(next(box), "╭─────╮")
        self.assertEqual(next(box), "│ foo │")
        self.assertEqual(next(box), "╰─────╯")
        with self.assertRaises(StopIteration):
            next(box)

    def test_multiple_labels(self):
        box = Box('long name', 'foo').get()
        self.assertEqual(next(box), "╭───────────╮")
        self.assertEqual(next(box), "│ long name │")
        self.assertEqual(next(box), "│    foo    │")
        self.assertEqual(next(box), "╰───────────╯")

    def test_unalignable(self):
        box = Box('fo', 'sho').get()
        self.assertEqual(next(box), "╭─────╮")
        self.assertEqual(next(box), "│ fo  │")
        self.assertEqual(next(box), "│ sho │")
        self.assertEqual(next(box), "╰─────╯")
        with self.assertRaises(StopIteration):
            next(box)

    def test_str(self):
        box = Box('foo')
        self.assertEqual(
            str(box),
            """\
╭─────╮
│ foo │
╰─────╯""")
